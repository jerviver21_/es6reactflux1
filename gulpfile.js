var gulp 			= require('gulp');
var babel        	= require('gulp-babel');
var sass 			= require('gulp-sass');
var fontAwesome 	= require('node-font-awesome');
var sourcemaps 		= require('gulp-sourcemaps');
var autoprefixer 	= require('gulp-autoprefixer');
var notify       	= require('gulp-notify');
var plumber      	= require('gulp-plumber');
var react 			= require('gulp-react');
var browserify 		= require('browserify');
var vinyl 			= require('vinyl-source-stream');
var jasmine			= require('gulp-jasmine');
var browserSync 	= require('browser-sync');
var reload 			= browserSync.reload;

//Main source files of the project
var jsDestFile = "bundle.js"
//For development the destFolder is the one for browser-sync
var destFolder = ".tmp"

//Notification of errors with the use of plumber
var onError = function(err) {
	notify.onError({
		title:    "Error",
		message:  "<%= error %>",
	})(err);
	this.emit('end');
};

var plumberOptions = {
	errorHandler: onError,
};

//This task compiles jsx and es6 sources and concat them in one source file bundle.js
gulp.task('jsx', function() {
	return 	gulp.src(['src/**/*.js', 'src/**/*.jsx'])
			.pipe(plumber(plumberOptions))
			.pipe(sourcemaps.init())
			.pipe(react({harmony: false, es6module: true}))
			.pipe(babel())
			.pipe(gulp.dest('.tmp'))
			.pipe(sourcemaps.write());
});

gulp.task('browserify', ['jsx'], function() {
  return browserify(".tmp/js/main.js")
  .bundle()
  .pipe(vinyl(jsDestFile))
  .pipe(gulp.dest(destFolder+"/js"))
  .pipe(reload({ stream: true }));
});

//This task compiles the scss styles
gulp.task('sass', ['fonts'],function() {
	return gulp.src('src/styles/style.scss')
		.pipe(plumber(plumberOptions))
    	.pipe(sourcemaps.init())
		.pipe(sass()) 
        .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false})) 
		.pipe(gulp.dest('.tmp/styles'))
    	.pipe(sourcemaps.write())
		.pipe(reload({ stream: true })); // Recargar cambios al navegador
});

gulp.task('fonts', function() {
  gulp.src(fontAwesome.fonts)
    .pipe(gulp.dest('.tmp/fonts/font-awesome'));
});


//This task compile code if any change happen
gulp.task('watch', function() {
	gulp.watch('src/styles/style.scss', ['sass']);
	gulp.watch('src/js/**/*.{js,jsx}', ['browserify']); 
  	gulp.watch('src/*html').on('change', reload); 
});

gulp.task('serve', ['sass', 'browserify'], function() {
	browserSync({
		server: {
			baseDir: ['.tmp', 'src']
		}
	});
    gulp.start('watch');
});



gulp.task('default', ['serve']);