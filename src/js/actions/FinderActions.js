import dispatcher from "../dispatcher";


export function findRepos(params){
	var url = 'https://api.github.com/users/'+params.usuario+'/repos?sort=updated';
    if(params.incluirMiembro){
        url += '&type=all';
    }

    fetch(url).then(function(response){
      if(response.ok){
        response.json().then(function(body){
        	dispatcher.dispatch(
        						{
        							type: "RECEIVE_REPOS",
									resultados:body
								});

        });
      }else{
          dispatcher.dispatch(
        						{
        							type: "NO_REPOS",
									resultados:{}
								});
      }
    });
}

