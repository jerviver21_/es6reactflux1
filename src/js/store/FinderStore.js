import EventEmmiter from "events";

import dispatcher from "../dispatcher";

class FinderStore extends EventEmmiter{
	constructor(){
		super();
		this.resultados = [];
	}

	getRepos(){
		return this.resultados;
	}

	handleActions(action){
		switch(action.type){
			case "RECEIVE_REPOS":{
				this.resultados = action.resultados;
				break;
			}
			case "NO_REPOS":{
				this.resultados = [];
				break;
			}
		}
		
		this.emit("change");
	}

}

const finderStore = new FinderStore();
dispatcher.register(finderStore.handleActions.bind(finderStore));

export default finderStore;