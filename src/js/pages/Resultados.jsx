import React from "react";
import ItemResultado from "./ItemResultado";

export default class Resultados extends React.Component{
  render(){
    return <ul className="list-group">
      {this.props.resultados.map(function(resultado){
        return <ItemResultado key={resultado.id} resultado={resultado}/>;
      }.bind(this))}
    </ul>;
  }
}