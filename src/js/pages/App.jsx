import React from "react";
import Menu from "./Menu";

export default class App extends React.Component{

	render(){
		return(
			<div>
				<Menu/>
				<hr/>
				{this.props.children}
			</div>
		)
	}

}