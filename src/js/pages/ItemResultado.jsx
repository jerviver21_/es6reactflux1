import React from "react";
import moment from "moment";


export default class ItemResultado extends React.Component {
  render(){
    var resultado = this.props.resultado;
    return <li className="list-group-item">
       <div className="row">
          <div className="col-lg-10">
          <h4>
              <a href={resultado.html_url} target="blank">
                {resultado.name}
              </a> {resultado.private && <span className="resultado-privado">Privado</span>}
            </h4>
            <p className="resultado-info">
                {
                  resultado.fork && <span className="resultado-fork">
                    <i className="fa fa-code-fork"/>Forkeado
                  </span>
                }
            </p>
            <p className="resultado-descripcion">{resultado.descripcion}</p>
            <p className="resultado-actualizado">Actualizado {moment(resultado.updated_at).fromNow()}</p>
          </div>

          <div className="col-lg-2 resultado-stats">
            <span className="resultado-stat">
              {resultado.language}&nbsp;I
            </span>
            <span className="resultado-stat">
              <i className="fa fa-code-fork"/>{resultado.forks_count}
            </span>
            <span className="resultado-stat">
              <i className="fa fa-star"/>{resultado.stargazers_count}
            </span>
            <span className="resultado-stat">
              <i className="fa fa-eye"/>{resultado.watchers_count}
            </span>
          </div>
        </div>
      </li>;
  }
}