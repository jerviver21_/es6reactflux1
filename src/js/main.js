import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import App from "./pages/App";
import Home from "./pages/Home";
import Finder from "./pages/Finder";

const app = document.getElementById("content");

ReactDOM.render(<Router history={hashHistory}>
					<Route path="/" component={App}>
						<IndexRoute component={Home}/>
						<Route path="finder" component={Finder} />
					</Route>
				</Router>, 	app);